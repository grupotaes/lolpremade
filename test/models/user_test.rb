require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: 'Usuario ejemplo',
                     email: 'ejemplo@ejemplo.com',
                     password: 'password',
                     password_confirmation: 'password')
  end

  test "debe ser valido" do
    assert @user.valid?
  end

  test "el nombre debe estar presente" do
    @user.name = ''
    assert_not @user.valid?
  end

  test "el email debe estar presente" do
    @user.email = ''
    assert_not @user.valid?
  end

  test "el email no debe ser demasiado largo" do
    @user.email = 'a' * 244 + '@example.com'
    assert_not @user.valid?
  end

# %w[hola adios buenas] es un array de string, sería equivalente a
# ["hola", "adios", "buenas"]
  test "la validacion del email debe aceptar direcciones validas" do
    direcciones_validas = %w[user@example.com USER@foo.COM A_US@foo.bar.org
                          first.last@foo.jp alice+bob@baz.com]

    direcciones_validas.each do |direccion_valida|
      @user.email = direccion_valida
      assert @user.valid?, "#{direccion_valida.inspect} debe ser valida"
    end
  end

  # Estos dos test lo que hacen es dado un array de strings
  # luego recorren ese array y asignan cada string a la dirección
  # de email del usuario y comprueba si es valida.
  # expresion regular que se puede usar para validar el email
  # /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  test "la validacion del email debe impedir direcciones invalidas" do
    direcciones_invalidas = %w[user@example,com user_at_fo.org user.name@example.
                               foo@bar_baz.com foo@bar+baz.com]

    direcciones_invalidas.each do |direccion_invalida|
      @user.email = direccion_invalida
      assert_not @user.valid?, "#{direccion_invalida.inspect} debe ser invalida"
    end
  end
end
