// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$('document').ready(function() {
  $('#people-filter').on('ajax:success', function (e, data, status, xhr) {
    $('#people').html(xhr.responseText);
  }).on('ajax:error', function (e, xhr, status, error) {
    $('#people-filter').append(error);
  });
});