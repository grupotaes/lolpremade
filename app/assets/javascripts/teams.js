// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on("page:change", function() {
    $('#teams-filter').on('ajax:success', function (e, data, status, xhr) {
        $('#teams-table').html(xhr.responseText);
    }).on('ajax:error', function (e, xhr, status, error) {
        $('#teams-filter').append(error);
    });
});