// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on("page:change", function() {
    $('#users-filter').on('ajax:success', function (e, data, status, xhr) {
        $('#users-table').html(xhr.responseText);
    }).on('ajax:error', function (e, xhr, status, error) {
        $('#users-filter').append(error);
    });
});