class MessagesController < ApplicationController

  def create
    @team = Team.find(params[:message][:team])
    @team.team_messages.create message_params
    redirect_to @team
  end

  def create_user_message
    @user = User.find(params[:message][:user])
    @user.user_messages.create message_params
    redirect_to @user
  end

  private
  def message_params
    params.require(:message).permit(:body, :author, :author_name)
  end
end
