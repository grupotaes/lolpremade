class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      remember(user)
      redirect_to user
    else
      # Crear un mensaje de error
      flash[:danger] = 'Combinación mail/password inválida'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
