class UsersController < ApplicationController

  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user,   only: [:edit, :update]


  def index
    @league = %w[Unranked Bronze Silver Gold Platinum Diamond Master Challenger]
    @divisions = %w[I II III IV V]
    @servers = %w[EUW EUN&E NA LAN LAS OCEANIA BRAZIL KOREA EUN JAPAN RUSSIA TURKEY ]
    @genders = %w[Male Female ]
    @voip = %w[Y N ]
    @users = User.all
  end

  def filter
    @users = User.filter(filter_params)
    render partial: 'shared/users'
    # render json: @users
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = 'Bienvenido a LOLPremade'
      redirect_to @user
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
    @messages = @user.user_messages
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params_edit)

      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end

  end

  private

    def filter_params
      params.require(:filter).permit(:user_name, :summoner_name, :league, :sex, :hometown, :country, :server, :age, :level, :division, :voip, :level)
    end

    def user_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def user_params_edit
        params.require(:user).permit(:name, :password, :password_confirmation, :summoner_name, :image, :hometown, :country, :sex, :birthdate, :server, :league, :division, :level, :languages)
    end

    def logged_in_user
        unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
   end

   # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end

end
