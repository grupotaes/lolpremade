class TeamsController < ApplicationController
    def index #get teams
        @teams = Team.all
    end

    def show #get by /teams/id
        @team = Team.find(params[:id])
        @users = @team.users
        @messages = @team.team_messages
    end

    def new
        @team=Team.new
    end

    def create
        @user = User.find(current_user.id)
        @team = Team.new(team_params)
        if @team.save
            @user.teams << @team
            redirect_to @team
        else
            render 'new'
        end
    end

    def filter
        @teams = Team.filter(filter_params)
        render partial: 'shared/teams'
    end

    def add_user
        @team = Team.find(params[:add_member][:team])
        user = User.find_by(email: params[:add_member][:email])
        if user && !@team.users.exists?(user)
            @team.users << user
        end

        redirect_to @team
    end

    def showbyuser
        @usuario = User.find(current_user.id)
        @teams = @usuario.teams
    end

    def edit
        @team = Team.find(params[:id])
    end


    def update
        @team = Team.find(params[:id])
        @user = User.find(current_user.id)


        #if @user.teams.include? @team )
         #   render 'show'

        #else


            @user.teams << @team
            redirect_to @team

    end

    def team_params
        params.require(:team).permit(:teamname)
    end

    def filter_params
        params.require(:filter).permit(:teamname)
    end
end
