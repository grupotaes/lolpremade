class HomeController < ApplicationController

  def index
    @users = User.limit(9)
    @teams = Team.limit(3)
  end
end
