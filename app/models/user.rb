class User < ActiveRecord::Base
  has_and_belongs_to_many :teams
  has_many :user_messages

  include Filterable

  attr_accessor :remember_token

  validates :name, presence: true, length: { maximum: 50 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,
            presence: true,
            length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }

  has_secure_password

  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  scope :user_name, -> (name) { where("lower(name) LIKE ?", "%#{name}%".downcase) }
  scope :summoner_name, -> (summoner_name) { where("lower(summoner_name) LIKE ?", "%#{summoner_name}%".downcase) }
  scope :server, -> (server) { where("lower(server) LIKE ?", "%#{server}%".downcase) }
  scope :level, -> (level) { where("lower(level) LIKE ?", "%#{level}%".downcase) }
  scope :sex, -> (sex) { where sex: sex }
  scope :voip, -> (voip) { where voip: voip }
  scope :hometown, -> (hometown) { where("lower(hometown) LIKE ?", "%#{hometown}%".downcase) }
  scope :country, -> (country) { where("lower(country) LIKE ?", "%#{country}%".downcase) }
  scope :division, -> (division) { where("lower(division) LIKE ?", "#{division}".downcase) }
  scope :level, -> (level) { where("lower(level) LIKE ?", "#{level}".downcase) }
  scope :league, -> (league) { where("lower(league) LIKE ?", "#{league}".downcase) }


  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def forget
    update_attribute(:remember_token, nil)
  end

  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password? remember_token
  end


end
