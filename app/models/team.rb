class Team < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_many :team_messages
  include Filterable


  validates :teamname,
            presence: true,
            length: { maximum: 50 },
            uniqueness: { case_sensitive: false }

  scope :teamname, -> (teamname) { where("lower(teamname) LIKE ?", "%#{teamname}%".downcase) }
end
