class AddUserNameToMessages < ActiveRecord::Migration
  def change
    add_column :team_messages, :author_name, :string
    add_column :user_messages, :author_name, :string
  end
end
