class ActualizarBd < ActiveRecord::Migration
  def change
  	add_column :users, :image, :string
  	add_column :users, :languages, :string
  	change_column :users, :birthdate, :date
  end
end
