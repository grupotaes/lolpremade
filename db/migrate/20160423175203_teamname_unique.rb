class TeamnameUnique < ActiveRecord::Migration
  def change
  	add_index :teams, :teamname, unique: true
  end
end
