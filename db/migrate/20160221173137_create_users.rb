class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :hometown
      t.string :country
      t.string :sex
      t.string :birthdate
      t.string :voip
      t.string :coach
      t.string :description
      t.string :summoner_name
      t.string :contact
      t.string :time
      t.string :division
      t.string :level

      t.timestamps null: false
    end

    create_table :teams do |t|
      t.string :name

      t.timestamps null: false
    end

    create_table :teams_users do |t|
      t.belongs_to :user, index: true
      t.belongs_to :team, index: true
    end

    create_table :team_messages do |t|
      t.belongs_to :team, index: true
      t.string :body
      t.integer :author

      t.timestamps null: false
    end

    create_table :user_messages do |t|
      t.belongs_to :user, index: true
      t.string :body
      t.integer :author

      t.timestamps null: false
    end
  end
end
