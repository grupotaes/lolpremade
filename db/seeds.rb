# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Team.delete_all
team = Team.create! teamname: 'lolpremade', id: 1

User.delete_all
manu = User.create! name: 'manu',
             email: 'x@gmail.com',
             password: 'password',
             password_confirmation: 'password',
             hometown: 'Elche',
             sex: 'hombre',
             description: 'Era un hombre que un día se levanto y...',
             league: 'Challenger',
             image: 'osito'

 potatoe = User.create! name: 'sweet potatoe',
             email: 'potatoe@gmail.com',
             password: 'password',
             password_confirmation: 'password',
             hometown: 'Elche',
             sex: 'hombre',
             description: 'I\'m a potatoe',
             league: 'Bronze',
             division: 'II',
             image: 'hielo'

lobito = User.create! name: 'lobito',
          email: 'lobo@lobo.com',
          password: 'password',
          password_confirmation: 'password',
          hometown: 'El bosque',
          sex: 'hombre',
          description: 'I\'m a wooolf',
          league: 'Gold',
          division: 'III',
          image: 'corazon'

levels = %w[Bronze Silver Gold Platinum Diamond Master Challenger]
divisions = %w[I II III IV V]
images = %w[corazon hielo manos osito osito2 ]


TeamMessage.delete_all
Team.create! teamname: 'team1'
Team.create! teamname: 'team2'

team.users << manu
team.users << potatoe

UserMessage.delete_all
lobito.user_messages.create! body: '¿Hola lobo como estás?',
                           author: manu.id,
                           author_name: manu.name

team.team_messages.create! body: "Este es un mensaje para el grupo Lolpreamade",
                           author: manu.id,
                           author_name: manu.name

team.team_messages.create! body: "Este es otro mensaje para el mismo grupo",
                           author: potatoe.id,
                           author_name: potatoe.name

19.times do |n|
  name = Faker::Name.name
  email = "usuario-#{n+1}@lolpremade.com"
  password = 'password'
  hometown = Faker::Address.city
  description = Faker::Lorem.paragraph

  User.create! name: name,
               email: email,
               password: password,
               password_confirmation: password,
               hometown: hometown,
               description: description,
               league: levels.sample,
               division: divisions.sample,
               image: images.sample
end
