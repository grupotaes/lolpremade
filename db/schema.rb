# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160425155244) do

  create_table "team_messages", force: :cascade do |t|
    t.integer  "team_id"
    t.string   "body"
    t.integer  "author"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "author_name"
  end

  add_index "team_messages", ["team_id"], name: "index_team_messages_on_team_id"

  create_table "teams", force: :cascade do |t|
    t.string   "teamname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "teams", ["teamname"], name: "index_teams_on_teamname", unique: true

  create_table "teams_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "team_id"
  end

  add_index "teams_users", ["team_id"], name: "index_teams_users_on_team_id"
  add_index "teams_users", ["user_id"], name: "index_teams_users_on_user_id"

  create_table "user_messages", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "body"
    t.integer  "author"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "author_name"
  end

  add_index "user_messages", ["user_id"], name: "index_user_messages_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "hometown"
    t.string   "country"
    t.string   "sex"
    t.date     "birthdate"
    t.string   "voip"
    t.string   "coach"
    t.string   "description"
    t.string   "summoner_name"
    t.string   "contact"
    t.string   "time"
    t.string   "division"
    t.string   "level"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "image"
    t.string   "languages"
    t.string   "league"
    t.string   "server"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
